package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.gitlab.robert.cebula.camelight_core.BoardElement;
import com.gitlab.robert.cebula.camelight_core.Camel;
import com.gitlab.robert.cebula.camelight_core.EmptyBoardElement;
import com.gitlab.robert.cebula.camelight_core.Plate;

class Field {
    private final Rect rect;
    private final TextPaint plateTextPaint;
    private final Paint camelPaint;
    private final Paint borderPlatePaint;
    private final Paint camelPaintBorder;
    private final Paint fillPlatePaint;
    private final Paint stepChanceFillPaint;
    private final Paint stepChanceBorderPaint;
    private final TextPaint stepChanceTextPaint;

    private DrawElement drawElement;

    Field(Rect rect, TextPaint plateTextPaint, Paint camelPaint, Paint borderPlatePaint,
          Paint camelPaintBorder, Paint fillPlatePaint, Paint stepChanceFillPaint,
          Paint stepChanceBorderPaint, TextPaint stepChanceTextPaint) {
        this.rect = rect;
        this.plateTextPaint = plateTextPaint;
        this.camelPaint = camelPaint;
        this.borderPlatePaint = borderPlatePaint;
        this.camelPaintBorder = camelPaintBorder;
        this.fillPlatePaint = fillPlatePaint;
        this.stepChanceFillPaint = stepChanceFillPaint;
        this.stepChanceBorderPaint = stepChanceBorderPaint;
        this.stepChanceTextPaint = stepChanceTextPaint;
    }

    void updateDrawElement(BoardElement boardElement, int stepChance) {
        if (boardElement instanceof EmptyBoardElement) {
            this.drawElement = new EmptyElement(stepChance, this.rect, stepChanceFillPaint, stepChanceBorderPaint, stepChanceTextPaint);
            return;
        }

        if (boardElement instanceof Plate) {
            Plate plate = (Plate) boardElement;
            this.drawElement = new PlateElement(plate.platePositionChange(), this.rect,
                    borderPlatePaint, fillPlatePaint, plateTextPaint, stepChanceFillPaint,
                    stepChanceBorderPaint, stepChanceTextPaint, stepChance);
            return;
        }

        if (boardElement instanceof Camel) {
            Camel camel = (Camel) boardElement;
            this.drawElement = new CamelElement(camel.thisAndUpperCamelsColors(), this.rect, camelPaint, camelPaintBorder);
        }
    }

    void draw(Canvas canvas) {
        this.drawElement.draw(canvas);
    }

    boolean isClicked(int x, int y) {
        return this.rect.contains(x, y);
    }
}
