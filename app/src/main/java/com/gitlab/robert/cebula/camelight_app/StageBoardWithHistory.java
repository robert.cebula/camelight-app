package com.gitlab.robert.cebula.camelight_app;

import com.gitlab.robert.cebula.camelight_core.Board;
import com.gitlab.robert.cebula.camelight_core.BoardElement;
import com.gitlab.robert.cebula.camelight_core.BoardImpl;
import com.gitlab.robert.cebula.camelight_core.Dice;
import com.gitlab.robert.cebula.camelight_core.GameState;
import com.gitlab.robert.cebula.camelight_core.Plate;
import com.gitlab.robert.cebula.camelight_core.StageBoard;

import java.util.ArrayList;
import java.util.List;
import com.gitlab.robert.cebula.camelight_core.Optional;

interface BoardUpdateListener {
    void onUpdate(StageBoard current);
}

interface HistoryUpdateListener {
    void onUpdate(boolean undoEnabled, boolean redoEnabled);
}

interface PlatesUpdateListener {
    void onUpdate(boolean containsPlates);
}

public class StageBoardWithHistory implements Board {
    // history contains current as last element
    private final List<StageBoard> history = new ArrayList<>();
    private StageBoard current;
    private int positionInHistory;

    private final List<BoardUpdateListener> boardUpdateListeners = new ArrayList<>();
    private final List<HistoryUpdateListener> historyUpdateListeners = new ArrayList<>();
    private final List<PlatesUpdateListener> platesUpdateListeners = new ArrayList<>();

    StageBoardWithHistory(StageBoard current) {
        this.current = current;
        this.history.add(this.current);
        positionInHistory = 0;
    }

    void undo() {
        if (positionInHistory == 0) {
            return;
        }

        --positionInHistory;
        this.current = this.history.get(positionInHistory);
        callListeners();
    }

    void redo() {
        if (positionInHistory == this.history.size() - 1) {
            return;
        }

        ++positionInHistory;
        this.current = this.history.get(positionInHistory);
        callListeners();
    }

    void addBoardUpdateListener(BoardUpdateListener boardUpdateListener) {
        this.boardUpdateListeners.add(boardUpdateListener);
    }

    void addBoardUpdateListenerAtTheBeginning(BoardUpdateListener boardUpdateListener) {
        this.boardUpdateListeners.add(0, boardUpdateListener);
    }

    void addHistoryUpdateListener(HistoryUpdateListener historyUpdateListener) {
        this.historyUpdateListeners.add(historyUpdateListener);
    }

    void addPlatesUpdateListener(PlatesUpdateListener platesUpdateListener) {
        this.platesUpdateListeners.add(platesUpdateListener);
    }

    StageBoard currentStageBoard() {
        return this.current;
    }

    BoardImpl currentBoardImpl() {
        return this.current.boardImpl();
    }

    private void callListeners() {
        for (BoardUpdateListener boardUpdateListener : boardUpdateListeners) {
            boardUpdateListener.onUpdate(this.current);
        }

        boolean undoEnabled = positionInHistory > 0;
        boolean redoEnabled = positionInHistory < this.history.size() - 1;
        for (HistoryUpdateListener historyUpdateListener : this.historyUpdateListeners) {
            historyUpdateListener.onUpdate(undoEnabled, redoEnabled);
        }

        boolean containsPlates = current.containsPlates();
        for (PlatesUpdateListener platesUpdateListener : this.platesUpdateListeners) {
            platesUpdateListener.onUpdate(containsPlates);
        }
    }

    private void copyCurrent() {
        performBoardUpdate(new StageBoard(this.current));
    }

    @Override
    public boolean containsPlates() {
        return this.current.containsPlates();
    }

    @Override
    public boolean removePlates() {
        StageBoard potentiallyNew = new StageBoard(this.current);
        if (potentiallyNew.removePlates()) {
            performBoardUpdate(potentiallyNew);

            callListeners();
            return true;
        }

        return false;
    }

    @Override
    public Optional<Plate> getPlateAtPosition(int position) {
        return this.current.getPlateAtPosition(position);
    }

    @Override
    public boolean addOrSwitchPlateAtPosition(Plate plate, int position) {
        StageBoard potentiallyNew = new StageBoard(this.current);
        if (potentiallyNew.addOrSwitchPlateAtPosition(plate, position)) {
            performBoardUpdate(potentiallyNew);

            callListeners();
            return true;
        }

        return false;
    }

    private void performBoardUpdate(StageBoard newStageBoard) {
        this.history.subList(positionInHistory + 1, this.history.size()).clear();
        this.current = newStageBoard;
        this.history.add(this.current);
        positionInHistory = this.history.size() - 1;
    }

    @Override
    public void removePlateAtPosition(int position) {
        copyCurrent();
        this.current.removePlateAtPosition(position);
        callListeners();
    }

    @Override
    public void move(Dice dice) {
        copyCurrent();
        this.current.move(dice);
        callListeners();
    }

    @Override
    public boolean tryToMoveUpperCamel(int startingPosition, int endingPosition) {
        StageBoard potentiallyNew = new StageBoard(this.current);
        if (potentiallyNew.tryToMoveUpperCamel(startingPosition, endingPosition)) {
            performBoardUpdate(potentiallyNew);

            callListeners();
            return true;
        }

        return false;
    }

    @Override
    public GameState currentGameState() {
        return this.current.currentGameState();
    }

    @Override
    public BoardElement getBoardElement(int position) {
        return this.current.getBoardElement(position);
    }
}
