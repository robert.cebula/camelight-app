package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Probabilities;
import com.gitlab.robert.cebula.camelight_core.Probability;
import com.gitlab.robert.cebula.camelight_core.RaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageProbabilities;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.MAX_CAMEL_STACK;

public class ProbabilitiesPageFragment extends AbstractPageFragment {

    private LinearLayout resultsLayout;

    private final StageBoardWithHistory stageBoardWithHistory;

    private List<TextView> raceWinnerResultsTextViews = new ArrayList<>();
    private List<TextView> stageResultsTextViews = new ArrayList<>();
    private List<TextView> raceLooserResultsTextViews = new ArrayList<>();

    ProbabilitiesPageFragment(StageBoardWithHistory stageBoardWithHistory,
                              StageProbabilities stageProbabilities) {
        super(stageProbabilities);
        this.stageBoardWithHistory = stageBoardWithHistory;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.results_layout, container, false);

        resultsLayout = rootView.findViewById(R.id.results_layout);

        this.createResultsLayout(this.stageProbabilities);

        return rootView;
    }

    private void createResultsLayout(StageProbabilities stageProbabilities) {
        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT,
                1);
        tableParams.weight = 1.0f;

        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,
                1);
        rowParams.weight = 1.0f;

        TableLayout tableLayout = new TableLayout(getActivity());
        tableLayout.setWeightSum(6.0f);
        tableLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        for (int row = 0; row < MAX_CAMEL_STACK + 1; ++row) {
            TableRow tableRow = new TableRow(getActivity());
            tableRow.setWeightSum(3.0f);
            tableRow.setLayoutParams(tableParams);

            TextView stageResultTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                stageResultTextView.setTextColor(android.graphics.Color.WHITE);
                stageResultTextView.setText("STAGE WINNER");
                stageResultTextView.setTextSize(15.0f);
                stageResultTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                stageResultsTextViews.add(stageResultTextView);
            }

            TextView raceWinnerResultTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                raceWinnerResultTextView.setTextColor(android.graphics.Color.WHITE);
                raceWinnerResultTextView.setText("RACE WINNER");
                raceWinnerResultTextView.setTextSize(15.0f);
                raceWinnerResultTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                raceWinnerResultsTextViews.add(raceWinnerResultTextView);
            }

            TextView raceLooserResultTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                raceLooserResultTextView.setTextColor(Color.WHITE);
                raceLooserResultTextView.setText("RACE LOOSER");
                raceLooserResultTextView.setTextSize(15.0f);
                raceLooserResultTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                raceLooserResultsTextViews.add(raceLooserResultTextView);
            }

            tableLayout.addView(tableRow);
        }

        resultsLayout.addView(tableLayout);

        updateStageResultsLayout(stageProbabilities);
    }

    private TextView createTextView(TableRow.LayoutParams rowParams, TableRow tableRow) {
        TextView textView = new TextView(getActivity());
        textView.setLayoutParams(rowParams);
        textView.setTextColor(android.graphics.Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(19.0f);
        tableRow.addView(textView);
        return textView;
    }

    private void updateStageResultsLayout(StageProbabilities stageProbabilities) {
        Probabilities probabilities = stageProbabilities.winnersProbabilities();
        List<Probability> sortedProbabilities = probabilities.getCopyOfProbabilities();
        Collections.sort(sortedProbabilities);

        for (int it = 0; it < sortedProbabilities.size(); ++it) {
            Probability probability = sortedProbabilities.get(sortedProbabilities.size() - it - 1);

            CamelColor camelColor = probability.camelColor();
            int probabilityInPercent = Math.round(probability.probability() * 100.0f);
            TextView textView = stageResultsTextViews.get(it);

            textView.setBackgroundColor(CamelToColorAndroidColorMap.get(camelColor).normal);
            textView.setText(probabilityInPercent + "%");
        }
    }

    private void updateRaceResultsLayout(RaceProbabilities raceProbabilities) {
        updateRaceResultsOneColumn(raceProbabilities.loosersProbabilities(), raceLooserResultsTextViews);
        updateRaceResultsOneColumn(raceProbabilities.winnersProbabilities(), raceWinnerResultsTextViews);
    }

    private void updateRaceResultsOneColumn(Probabilities probabilities, List<TextView> textViews) {
        List<Probability> sortedProbabilities = probabilities.getCopyOfProbabilities();
        Collections.sort(sortedProbabilities);

        for (int it = 0; it < sortedProbabilities.size(); ++it) {
            Probability probability = sortedProbabilities.get(sortedProbabilities.size() - it - 1);

            CamelColor camelColor = probability.camelColor();
            int probabilityInPercent = Math.round(probability.probability() * 100.0f);
            TextView textView = textViews.get(it);

            textView.setBackgroundColor(CamelToColorAndroidColorMap.get(camelColor).normal);
            textView.setText(probabilityInPercent + "%");
        }
    }

    @Override
    public void updateRaceProbabilities(RaceProbabilities raceProbabilities) {
        updateRaceResultsLayout(raceProbabilities);
    }

    @Override
    public void updateStageProbabilities(StageProbabilities stageProbabilities) {
        updateStageResultsLayout(stageProbabilities);
    }
}
