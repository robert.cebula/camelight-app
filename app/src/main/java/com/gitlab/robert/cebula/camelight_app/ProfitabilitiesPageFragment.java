package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Profitability;
import com.gitlab.robert.cebula.camelight_core.RaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageProfitabilities;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.MAX_CAMEL_STACK;

public class ProfitabilitiesPageFragment extends AbstractPageFragment {
    private LinearLayout resultsLayout;

    private final StageBoardWithHistory stageBoardWithHistory;

    private List<TextView> threeCoinsTextViews = new ArrayList<>();
    private List<TextView> fiveCoinsTextViews = new ArrayList<>();
    private List<TextView> twoCoinsTextViews = new ArrayList<>();

    ProfitabilitiesPageFragment(StageBoardWithHistory stageBoardWithHistory,
                                StageProbabilities stageProbabilities) {
        super(stageProbabilities);
        this.stageBoardWithHistory = stageBoardWithHistory;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.results_layout, container, false);

        resultsLayout = rootView.findViewById(R.id.results_layout);

        this.createResultsLayout(this.stageProbabilities);

        return rootView;
    }

    private void createResultsLayout(StageProbabilities stageProbabilities) {
        TableLayout.LayoutParams tableParams = new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT,
                1);
        tableParams.weight = 1.0f;

        TableRow.LayoutParams rowParams = new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,
                1);
        rowParams.weight = 1.0f;

        TableLayout tableLayout = new TableLayout(getActivity());
        tableLayout.setWeightSum(6.0f);
        tableLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));

        for (int row = 0; row < MAX_CAMEL_STACK + 1; ++row) {
            TableRow tableRow = new TableRow(getActivity());
            tableRow.setWeightSum(3.0f);
            tableRow.setLayoutParams(tableParams);

            TextView fiveCoinsTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                fiveCoinsTextView.setTextColor(android.graphics.Color.WHITE);
                fiveCoinsTextView.setText("5 COINS");
                fiveCoinsTextView.setTextSize(15.0f);
                fiveCoinsTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                fiveCoinsTextViews.add(fiveCoinsTextView);
            }

            TextView threeCoinsTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                threeCoinsTextView.setTextColor(android.graphics.Color.WHITE);
                threeCoinsTextView.setText("3 COINS");
                threeCoinsTextView.setTextSize(15.0f);
                threeCoinsTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                threeCoinsTextViews.add(threeCoinsTextView);
            }

            TextView twoCoinsTextView = createTextView(rowParams, tableRow);
            if (row == 0) {
                twoCoinsTextView.setTextColor(Color.WHITE);
                twoCoinsTextView.setText("2 COINS");
                twoCoinsTextView.setTextSize(15.0f);
                twoCoinsTextView.setBackgroundResource(R.drawable.white_border);
            } else {
                twoCoinsTextViews.add(twoCoinsTextView);
            }

            tableLayout.addView(tableRow);
        }

        resultsLayout.addView(tableLayout);

        updateProfitabilities(stageProbabilities);
    }

    private TextView createTextView(TableRow.LayoutParams rowParams, TableRow tableRow) {
        TextView textView = new TextView(getActivity());
        textView.setLayoutParams(rowParams);
        textView.setTextColor(android.graphics.Color.BLACK);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(19.0f);
        tableRow.addView(textView);
        return textView;
    }

    private void updateProfitabilities(StageProbabilities stageProbabilities) {
        StageProfitabilities stageProfitabilities = new StageProfitabilities(stageProbabilities);
        List<Profitability> profitabilities = stageProfitabilities.profitabilities();
        sortProfitabilities(profitabilities);

        for (int it = 0; it < profitabilities.size(); ++it) {
            Profitability profitability = profitabilities.get(it);

            TextView fiveCoinsTextView = fiveCoinsTextViews.get(it);
            TextView threeCoinsTextView = threeCoinsTextViews.get(it);
            TextView twoCoinsTextView = twoCoinsTextViews.get(it);

            setProfitabilityOnTextView(profitability, fiveCoinsTextView,
                    profitability.fiveCoinsPlateProfitability());
            setProfitabilityOnTextView(profitability, threeCoinsTextView,
                    profitability.threeCoinsPlateProfitability());
            setProfitabilityOnTextView(profitability, twoCoinsTextView,
                    profitability.twoCoinsPlateProfitability());
        }
    }

    interface Function<T, R> {
        R apply(T val);
    }

    private void sortProfitabilities(List<Profitability> profitabilities) {
        Collections.sort(profitabilities, (left, right) -> {
            Function<Profitability, Float> sumProfitabilities = profitability ->
                    profitability.fiveCoinsPlateProfitability()
                            + profitability.threeCoinsPlateProfitability()
                            + profitability.twoCoinsPlateProfitability();

            float leftSum = sumProfitabilities.apply(left);
            float rigtSum = sumProfitabilities.apply(right);

            return Float.compare(rigtSum, leftSum);
        });
    }

    private void setProfitabilityOnTextView(Profitability profitability, TextView textView,
                                            float profitabilityNumber) {
        CamelColor camelColor = profitability.camelColor();
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        textView.setBackgroundColor(CamelToColorAndroidColorMap.get(camelColor).normal);
        textView.setText(decimalFormat.format(profitabilityNumber));
    }

    @Override
    public void updateRaceProbabilities(RaceProbabilities raceProbabilities) {
        // do nothing
    }

    @Override
    public void updateStageProbabilities(StageProbabilities stageProbabilities) {
        updateProfitabilities(stageProbabilities);
    }
}