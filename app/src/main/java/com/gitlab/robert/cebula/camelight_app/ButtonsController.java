package com.gitlab.robert.cebula.camelight_app;

import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.widget.Button;

class ButtonsController {
    private final GameActivity gameActivity;
    private final Button newGameButton;
    private final Button undoButton;
    private final Button redoButton;
    private final Button removePlatesButton;

    private final MediaPlayer buttonClickPlayer;

    private final StageBoardWithHistory stageBoardWithHistory;

    ButtonsController(GameActivity gameActivity, Button newGameButton, Button undoButton,
                      Button redoButton, Button removePlatesButton,
                      StageBoardWithHistory stageBoardWithHistory) {
        this.gameActivity = gameActivity;
        this.newGameButton = newGameButton;
        this.undoButton = undoButton;
        this.redoButton = redoButton;
        this.removePlatesButton = removePlatesButton;
        this.stageBoardWithHistory = stageBoardWithHistory;

        this.buttonClickPlayer = MediaPlayer.create(gameActivity, R.raw.button_click);

        this.stageBoardWithHistory.addHistoryUpdateListener((undoEnabled, redoEnabled) -> {
            undoButton.setEnabled(undoEnabled);
            redoButton.setEnabled(redoEnabled);

            if (undoEnabled) {
                undoButton.setBackgroundResource(R.drawable.undo);
            } else {
                undoButton.setBackgroundResource(R.drawable.undo_disabled);
            }

            if (redoEnabled) {
                redoButton.setBackgroundResource(R.drawable.redo);
            } else {
                redoButton.setBackgroundResource(R.drawable.redo_disabled);
            }
        });

        this.stageBoardWithHistory.addPlatesUpdateListener(containsPlates -> {
            if (containsPlates) {
                removePlatesButton.setEnabled(true);
                removePlatesButton.setBackgroundResource(R.drawable.remove_plates_enabled);
            } else {
                removePlatesButton.setEnabled(false);
                removePlatesButton.setBackgroundResource(R.drawable.remove_plates_disabled);
            }
        });

        this.removePlatesButton.setOnClickListener(view -> {
            buttonClickPlayer.start();
            this.stageBoardWithHistory.removePlates();
        });

        this.undoButton.setOnClickListener(view -> {
            buttonClickPlayer.start();
            this.stageBoardWithHistory.undo();
        });

        this.redoButton.setOnClickListener(view -> {
            buttonClickPlayer.start();
            this.stageBoardWithHistory.redo();
        });

        this.newGameButton.setOnClickListener(view -> {
            buttonClickPlayer.start();
            new AlertDialog.Builder(gameActivity)
                    .setTitle("New game")
                    .setMessage("Are you sure you want to start new game?")
                    .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                        this.buttonClickPlayer.start();
                        this.gameActivity.startDiceActivity();
                    })
                    .setNegativeButton(android.R.string.no,
                            (dialog, which) -> this.buttonClickPlayer.start())
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        });
    }
}
