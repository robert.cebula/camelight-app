package com.gitlab.robert.cebula.camelight_app;

import android.content.Context;
import android.media.MediaPlayer;
import android.view.MotionEvent;

import com.gitlab.robert.cebula.camelight_core.BoardElement;
import com.gitlab.robert.cebula.camelight_core.Camel;
import com.gitlab.robert.cebula.camelight_core.CamelColor;

import com.gitlab.robert.cebula.camelight_core.Optional;

class TouchManager {
    private Integer startingFieldPosition;
    private Integer endingFieldPosition;

    private Float currentX;
    private Float currentY;

    private CamelColor camelColor;

    private Field[] fields;
    private final StageBoardWithHistory stageBoardWithHistory;
    private final BoardView boardView;
    private final MediaPlayer swishPlayer;

    TouchManager(Field[] fields, StageBoardWithHistory stageBoardWithHistory, BoardView boardView,
                 Context context) {
        this.fields = fields;
        this.stageBoardWithHistory = stageBoardWithHistory;
        this.boardView = boardView;

        this.swishPlayer = MediaPlayer.create(context, R.raw.swish);

        clear();
    }

    void onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            currentX = event.getX();
            currentY = event.getY();

            Optional<Integer> fieldPosition = getClickedFieldPosition(event);
            if (!fieldPosition.isPresent()) {
                return;
            }

            fieldPosition.ifPresent(position -> this.startingFieldPosition = position);

            BoardElement boardElement =
                    stageBoardWithHistory.getBoardElement(this.startingFieldPosition);
            if (boardElement instanceof Camel) {
                Camel camel = (Camel) boardElement;
                Camel mostUpperCamel = camel.mostUpperCamel();
                camelColor = mostUpperCamel.color();
            }
            return;
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            Optional<Integer> fieldPosition = getClickedFieldPosition(event);
            fieldPosition.ifPresent(position -> this.endingFieldPosition = position);

            if (camelColor != null && endingFieldPosition != null) {
                if (this.stageBoardWithHistory.tryToMoveUpperCamel(
                        startingFieldPosition, endingFieldPosition)) {
                    this.swishPlayer.start();
                }
            }

            clear();

            refreshBoardView();

            return;
        }

        if (event.getAction() == MotionEvent.ACTION_MOVE) {
            currentX = event.getX();
            currentY = event.getY();

            refreshBoardView();
            return;
        }
    }

    private void refreshBoardView() {
        this.boardView.updateMovingCamel(camelColor, currentX, currentY);
        this.boardView.invalidate();
    }

    private void clear() {
        startingFieldPosition = null;
        endingFieldPosition = null;
        currentX = null;
        currentY = null;
        camelColor = null;
    }

    private Optional<Integer> getClickedFieldPosition(MotionEvent event) {
        for (int it = 0; it < fields.length; ++it) {
            if (fields[it].isClicked((int)event.getX(), (int)event.getY())) {
                return Optional.of(it);
            }
        }

        return Optional.empty();
    }

}