package com.gitlab.robert.cebula.camelight_app;

import androidx.fragment.app.Fragment;

import com.gitlab.robert.cebula.camelight_core.StageProbabilities;

abstract class AbstractPageFragment extends Fragment implements RaceResultsUpdater {
    final StageProbabilities stageProbabilities;

    public AbstractPageFragment(StageProbabilities stageProbabilities) {
        this.stageProbabilities = stageProbabilities;
    }
}
