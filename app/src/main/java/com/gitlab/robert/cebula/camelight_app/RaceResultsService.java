package com.gitlab.robert.cebula.camelight_app;

import android.app.IntentService;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import androidx.annotation.NonNull;

import com.gitlab.robert.cebula.camelight_core.CamelRaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.RaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageBoard;

import java.util.concurrent.atomic.AtomicBoolean;

public class RaceResultsService extends IntentService {
    AtomicBoolean stopped = new AtomicBoolean(false);

    public RaceResultsService() {
        super(RaceResultsService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        stopped.set(false);

        final Messenger messenger = intent.getParcelableExtra("handler");
        final StageBoard stageBoard = (StageBoard) intent.getSerializableExtra("board");

        assert stageBoard != null;
        CamelRaceProbabilities camelRaceProbabilities = new CamelRaceProbabilities(stageBoard);

        for (int outerIt = 0; outerIt < 10; ++outerIt) {
            for (int innerIt = 0; innerIt < 10000; ++innerIt) {
                camelRaceProbabilities.iteration();
            }

            if (stopped.get()) {
                return;
            }

            RaceProbabilities raceProbabilities = camelRaceProbabilities.getRaceProbabilities();

            assert messenger != null;
            Message message = new Message();
            message.obj = raceProbabilities;
            try {
                messenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopped.set(true);
    }
}

class RaceResultsHandler extends Handler {
    private RaceResultsReceiver raceResultsReceiver;

    RaceResultsHandler(RaceResultsReceiver raceResultsReceiver) {
        this.raceResultsReceiver = raceResultsReceiver;
    }

    @Override
    public void handleMessage(@NonNull Message msg) {
        super.handleMessage(msg);
        raceResultsReceiver.onReceiveResult(msg);
    }
}

interface RaceResultsReceiver {
    void onReceiveResult(Message msg);
}