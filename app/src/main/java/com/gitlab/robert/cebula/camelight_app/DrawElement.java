package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Canvas;

interface DrawElement {
    void draw(Canvas canvas);
}
