package com.gitlab.robert.cebula.camelight_app;

class Color {
    int normal;
    int dimmed;

    Color(int normal, int dimmed) {
        this.normal = normal;
        this.dimmed = dimmed;
    }
}
