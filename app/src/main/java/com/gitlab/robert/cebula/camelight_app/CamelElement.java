package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import com.gitlab.robert.cebula.camelight_core.CamelColor;

import java.util.List;

import static com.gitlab.robert.cebula.camelight_core.BoardImpl.MAX_CAMEL_STACK;

public class CamelElement implements DrawElement {
    private final List<CamelColor> camelColors;
    private final Rect drawRect;
    private final Paint camelPaint;
    private final Paint camelPaintBorder;

    public static final float CAMEL_WIDTH_RATIO = 0.6f;
    public static final float CAMEL_HEIGHT_RATIO = 1.0f / MAX_CAMEL_STACK;

    CamelElement(List<CamelColor> camelColors, Rect drawRect, Paint camelPaint,
                 Paint camelPaintBorder) {
        this.camelColors = camelColors;
        this.drawRect = drawRect;
        this.camelPaint = camelPaint;
        this.camelPaintBorder = camelPaintBorder;
    }

    @Override
    public void draw(Canvas canvas) {
        int width = (int)(CAMEL_WIDTH_RATIO * this.drawRect.width());
        int height = (int)(CAMEL_HEIGHT_RATIO * this.drawRect.height());

        int x = (int)(this.drawRect.left + (this.drawRect.width() - width) * CAMEL_WIDTH_RATIO);
        for (int it = 0; it < camelColors.size(); ++it) {
            CamelColor camelColor = this.camelColors.get(it);
            camelPaint.setColor(CamelToColorAndroidColorMap.get(camelColor).normal);

            int y = this.drawRect.bottom - (height * (it + 1));
            canvas.drawRect(x, y, x + width, y + height, camelPaint);
            canvas.drawRect(x, y, x + width, y + height, camelPaintBorder);
        }
    }
}
