package com.gitlab.robert.cebula.camelight_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.gitlab.robert.cebula.camelight_core.CamelStageProbabilities;
import com.gitlab.robert.cebula.camelight_core.RaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageProbabilities;

public class ResultsController implements RaceResultsReceiver {

    private final ResultsViewPagerAdapter resultsViewPagerAdapter;

    private final StageBoardWithHistory stageBoardWithHistory;
    private final Activity activity;

    ResultsController(Activity activity,
                      FragmentManager fragmentManager,
                      ViewPager resultsViewPager,
                      StageBoardWithHistory stageBoardWithHistory,
                      BoardView boardView) {
        this.activity = activity;
        this.stageBoardWithHistory = stageBoardWithHistory;

        StageProbabilities stageProbabilities = countStageProbabilities(stageBoardWithHistory);
        resultsViewPagerAdapter = new ResultsViewPagerAdapter(fragmentManager,
                stageBoardWithHistory, stageProbabilities);
        resultsViewPager.setAdapter(resultsViewPagerAdapter);

        boardView.updateSteps(stageProbabilities.steps());

        startRaceResultService();

        this.stageBoardWithHistory.addBoardUpdateListenerAtTheBeginning(current -> {
            restartRaceResultService();

            StageProbabilities stageProbabilitiesInsideLambda = countStageProbabilities(stageBoardWithHistory);

            boardView.updateSteps(stageProbabilitiesInsideLambda.steps());

            for (RaceResultsUpdater raceResultsUpdater : resultsViewPagerAdapter.getRaceResultsUpdaters()) {
                raceResultsUpdater.updateStageProbabilities(stageProbabilitiesInsideLambda);
            }
        });
    }

    private StageProbabilities countStageProbabilities(StageBoardWithHistory stageBoardWithHistory) {
        CamelStageProbabilities camelStageProbabilities = new CamelStageProbabilities(
                stageBoardWithHistory.currentBoardImpl(), stageBoardWithHistory.currentStageBoard().leftColors());
        return camelStageProbabilities.countProbabilities();
    }

    @Override
    public void onReceiveResult(Message msg) {
        RaceProbabilities raceProbabilities = (RaceProbabilities) msg.obj;

        for (RaceResultsUpdater raceResultsUpdater : resultsViewPagerAdapter.getRaceResultsUpdaters()) {
            raceResultsUpdater.updateRaceProbabilities(raceProbabilities);
        }
    }

    private void restartRaceResultService() {
        stopRaceResultService();
        startRaceResultService();
    }

    private void stopRaceResultService() {
        Intent intent = new Intent(activity, RaceResultsService.class);
        activity.stopService(intent);
    }

    private void startRaceResultService() {
        Intent intent = new Intent(activity, RaceResultsService.class);

        Handler handler = new RaceResultsHandler(this);
        intent.putExtra("handler", new Messenger(handler));
        intent.putExtra("board", this.stageBoardWithHistory.currentStageBoard());
        activity.startService(intent);
    }
}

class ResultsViewPagerAdapter extends FragmentStatePagerAdapter {

    private static final int NUM_OF_PAGES = 2;

    private AbstractPageFragment[] fragments = new AbstractPageFragment[NUM_OF_PAGES];

    ResultsViewPagerAdapter(FragmentManager fm, StageBoardWithHistory stageBoardWithHistory,
                            StageProbabilities stageProbabilities) {
        super(fm);

        fragments[0] = new ProbabilitiesPageFragment(stageBoardWithHistory, stageProbabilities);
        fragments[1] = new ProfitabilitiesPageFragment(stageBoardWithHistory, stageProbabilities);
    }

    RaceResultsUpdater[] getRaceResultsUpdaters() {
        return fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return fragments[position];
    }

    @Override
    public int getCount() {
        return NUM_OF_PAGES;
    }
}
