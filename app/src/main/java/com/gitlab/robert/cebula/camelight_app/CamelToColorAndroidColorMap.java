package com.gitlab.robert.cebula.camelight_app;

import com.gitlab.robert.cebula.camelight_core.CamelColor;

import java.util.HashMap;
import java.util.Map;

class CamelToColorAndroidColorMap {
    final static Map<CamelColor, Color> camelColorToAndroidColorMap =
            new HashMap<CamelColor, Color>() {{
        put(CamelColor.BLUE, new Color(0xFF0080ff, 0x800080ff));
        put(CamelColor.GREEN, new Color(0xFF00FF00, 0x8000FF00));
        put(CamelColor.ORANGE, new Color(0xFFFFA500, 0x80FFA500));
        put(CamelColor.WHITE, new Color(0xFFFFFFFF, 0x80FFFFFF));
        put(CamelColor.YELLOW, new Color(0xFFFFFF00, 0x80FFFF00));
    }};

    static Color get(CamelColor camelColor) {
        return camelColorToAndroidColorMap.get(camelColor);
    }
}
