package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class EmptyElement implements DrawElement {
    private final int stepChance;
    private final Rect drawRect;
    private final Paint stepChanceFillPaint;
    private final Paint stepChanceBorderPaint;
    private final TextPaint stepChanceTextPaint;

    private final Rect textBounds = new Rect(); //don't new this up in a draw method

    EmptyElement(int stepChance, Rect drawRect, Paint stepChanceFillPaint, Paint stepChanceBorderPaint, TextPaint stepChanceTextPaint) {
        this.stepChance = stepChance;
        this.drawRect = drawRect;
        this.stepChanceFillPaint = stepChanceFillPaint;
        this.stepChanceBorderPaint = stepChanceBorderPaint;
        this.stepChanceTextPaint = stepChanceTextPaint;
    }

    @Override
    public void draw(Canvas canvas) {
        drawStepChanceRectangle(canvas);
    }

    private void drawStepChanceRectangle(Canvas canvas) {
        float width = this.drawRect.width() * 0.3f;
        float height = this.drawRect.height() * 0.15f;
        float x = this.drawRect.left + (this.drawRect.width() - width) * 0.5f;
        float y = this.drawRect.bottom - (height * 1.05f);

        RectF rectF = new RectF(x, y, x + width, y + height);

        Rect rect = new Rect();
        rectF.round(rect);
        this.stepChanceTextPaint.calculateTextSize(rect, "100%");

        canvas.drawRect(rectF, this.stepChanceFillPaint);
        canvas.drawRect(rectF, this.stepChanceBorderPaint);
        drawTextCentred(canvas, this.stepChanceTextPaint.paint(), this.stepChance + "%",
                rectF.centerX(), rectF.centerY());
    }

    private void drawTextCentred(Canvas canvas, Paint paint, String text, float cx, float cy){
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, cx - textBounds.exactCenterX(), cy - textBounds.exactCenterY(), paint);
    }
}
