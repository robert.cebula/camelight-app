package com.gitlab.robert.cebula.camelight_app;

import android.app.Activity;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;

import androidx.annotation.NonNull;

class FontUtil {
    static float calculateFontSize(@NonNull Rect textBounds, @NonNull Rect textContainer,
                                   @NonNull String text, @NonNull Paint textPaint) {
        int stage = 1;
        float textSize = 0;

        while(stage < 3) {
            if (stage == 1) textSize += 10;
            else
            if (stage == 2) textSize -= 1;

            textPaint.setTextSize(textSize);
            textPaint.getTextBounds(text, 0, text.length(), textBounds);

            textBounds.offsetTo(textContainer.left, textContainer.top);

            boolean fits = textContainer.contains(textBounds);
            if (stage == 1 && !fits) stage++;
            else
            if (stage == 2 &&  fits) stage++;
        }

        return textSize;
    }
}

class ScreenSize {
    private static final float SMALL_SCREEN_SIZE_BELOW = 700.0f;
    private static final float TINY_SCREEN_SIZE_BELOW = 550.0f;
    private static Float dpHeight = null;

    static boolean isSmallScreen() {
        if (dpHeight == null) {
            throw new AssertionError("You should first call init() method");
        }
        return dpHeight < SMALL_SCREEN_SIZE_BELOW;
    }

    static boolean isTinyScreen() {
        if (dpHeight == null) {
            throw new AssertionError("You should first call init() method");
        }
        return dpHeight < TINY_SCREEN_SIZE_BELOW;
    }

    static void init(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        float density  = activity.getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
    }
}

