package com.gitlab.robert.cebula.camelight_app;

import android.app.AlertDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.fragment.app.FragmentActivity;
import androidx.viewpager.widget.ViewPager;

import com.gitlab.robert.cebula.camelight_core.BoardImpl;
import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Dice;
import com.gitlab.robert.cebula.camelight_core.GameState;
import com.gitlab.robert.cebula.camelight_core.StageBoard;

import java.util.ArrayList;
import java.util.Arrays;

public class GameActivity extends FragmentActivity {

    private LinearLayout boardLayout;
    private LinearLayout dicesLayout;
    private BoardView boardView;
    private Button newGameButton;
    private Button undoButton;
    private Button redoButton;
    private Button removePlatesButton;

    private ViewPager resultsViewPager;

    private StageBoardWithHistory stageBoardWithHistory;

    private DicesInGameController dicesInGameController;
    private ResultsController resultsController;
    private ButtonsController buttonsController;

    private MediaPlayer buttonClickPlayer;
    private MediaPlayer swipePlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);

        ScreenSize.init(this);

        ArrayList<Dice> startingDices =
                (ArrayList<Dice>) getIntent().getSerializableExtra("startingDices");

        StringBuilder msg = new StringBuilder();
        assert startingDices != null;
        for (Dice dice : startingDices) {
            msg.append(dice.camelColor()).append(":").append(dice.value()).append("\n");
        }

        StageBoard startingStageBoard = new StageBoard(
                BoardImpl.constructBoard(startingDices),
                new ArrayList<>(Arrays.asList(CamelColor.values()))
        );
        this.stageBoardWithHistory = new StageBoardWithHistory(startingStageBoard);
        this.stageBoardWithHistory.addBoardUpdateListener(current -> {
            if (current.currentGameState().equals(GameState.END)) {
                AlertDialog alertDialog = new AlertDialog.Builder(this)
                        .setTitle("Game ends")
                        .setMessage("Do you want to start a new game or undo last move?")
                        .setNegativeButton("Undo", (dialog, which) -> {
                            this.buttonClickPlayer.start();
                            this.stageBoardWithHistory.undo();
                        })
                        .setPositiveButton("New game", (dialog, which) -> {
                            this.buttonClickPlayer.start();
                            startDiceActivity();
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setCancelable(false)
                        .create();
                WindowManager.LayoutParams wmlp = alertDialog.getWindow().getAttributes();
                wmlp.gravity = Gravity.BOTTOM;
                wmlp.y = 100;
                alertDialog.show();
            }
        });

        boardLayout = findViewById(R.id.board_layout);
        dicesLayout = findViewById(R.id.dices_in_game_layout);
        newGameButton = findViewById(R.id.new_game_button);
        undoButton = findViewById(R.id.undo_button);
        redoButton = findViewById(R.id.redo_button);
        removePlatesButton = findViewById(R.id.remove_plates_button);
        resultsViewPager = findViewById(R.id.results_view_pager);

        this.buttonClickPlayer = MediaPlayer.create(this, R.raw.button_click);
        this.swipePlayer = MediaPlayer.create(this, R.raw.swipe);

        this.resultsViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position) {
                swipePlayer.start();
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });

        constructBoardView();

        dicesInGameController = new DicesInGameController(stageBoardWithHistory, this, dicesLayout);
        resultsController = new ResultsController(this, getSupportFragmentManager(), resultsViewPager, stageBoardWithHistory, boardView);
        buttonsController = new ButtonsController(this, newGameButton, undoButton,
                redoButton, removePlatesButton, stageBoardWithHistory);

        dicesInGameController.createDicesLayout();
    }

    private void constructBoardView() {
        boardView = new BoardView(this, stageBoardWithHistory);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        boardView.setLayoutParams(layoutParams);

        boardLayout.addView(boardView);
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Exit game")
                .setMessage("Are you sure you want to exit the game?")
                .setNegativeButton(android.R.string.no,
                        (dialog, which) -> this.buttonClickPlayer.start())
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    this.buttonClickPlayer.start();
                    finish();
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void startDiceActivity() {
        Intent intent = new Intent(this, DiceActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
