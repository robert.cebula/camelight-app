package com.gitlab.robert.cebula.camelight_app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaPlayer;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Dice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.graphics.Color.*;

class DicesInGameController {
    private final StageBoardWithHistory stageBoardWithHistory;
    private final Context context;
    private final LinearLayout dicesLayout;

    private final MediaPlayer diceThrowPlayer;

    private Map<CamelColor, Button[]> buttonsByCamelColor = new HashMap<>();

    private Integer[] dicesDrawableIds = new Integer[] {
            R.drawable.dice_one,
            R.drawable.dice_two,
            R.drawable.dice_three
    };

    DicesInGameController(StageBoardWithHistory stageBoardWithHistory, Context context, LinearLayout dicesLayout) {
        this.stageBoardWithHistory = stageBoardWithHistory;
        this.context = context;
        this.dicesLayout = dicesLayout;

        this.stageBoardWithHistory.addBoardUpdateListener(
                current -> updateDices(current.leftColors(), current.usedDices())
        );

        this.diceThrowPlayer = MediaPlayer.create(context, R.raw.dice_throw);
    }

    void createDicesLayout() {
        for (CamelColor camelColor : CamelColor.values()) {
            createDicesRow(camelColor);
        }

        updateDices(this.stageBoardWithHistory.currentStageBoard().leftColors(),
                this.stageBoardWithHistory.currentStageBoard().usedDices());
    }

    private void createDicesRow(final CamelColor camelColor) {
        LinearLayout linearLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                0,
                1.0f);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setWeightSum(3.0f);

        Button[] buttonsInRow = new Button[Dice.MAX_VALUE];
        buttonsByCamelColor.put(camelColor, buttonsInRow);
        for (int it = 0; it < Dice.MAX_VALUE; ++it) {
            Button button = new Button(context);
            layoutParams = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f);
            layoutParams.setMargins(10, 10, 10, 10);
            button.setLayoutParams(layoutParams);
            buttonsInRow[it] = button;
            int diceValue = it + 1;
            button.setOnClickListener(view -> {
                diceThrowPlayer.start();
                diceClickedController(camelColor, diceValue);
            });
            linearLayout.addView(button);
        }

        dicesLayout.addView(linearLayout);
    }

    private void diceClickedController(CamelColor camelColor, int diceValue) {
        Dice dice = new Dice(camelColor, diceValue);
        stageBoardWithHistory.move(dice);
    }

    private void updateDices(List<CamelColor> leftCamelColors, List<Dice> usedDices) {
        List<CamelColor> usedCamelColors = new ArrayList<>(Arrays.asList(CamelColor.values()));
        usedCamelColors.removeAll(leftCamelColors);

        Map<CamelColor, Integer> usedDiceValueByCamelColor = new HashMap<>();
        for (Dice usedDice : usedDices) {
            usedDiceValueByCamelColor.put(usedDice.camelColor(), usedDice.value());
        }

        for (CamelColor camelColor : usedCamelColors) {
            Color color = CamelToColorAndroidColorMap.get(camelColor);
            Button[] buttons = buttonsByCamelColor.get(camelColor);
            for (int it = 0; it < buttons.length; ++it) {
                Button button = buttons[it];
                button.setEnabled(false);

                if (it + 1 == usedDiceValueByCamelColor.get(camelColor)) {
                    setButtonUsedDiceBackground(dicesDrawableIds[it], color.dimmed, button);
                } else {
                    setButtonUnusedDiceBackground(dicesDrawableIds[it], color.dimmed, button);
                }
            }
        }

        for (CamelColor camelColor : leftCamelColors) {
            Color color = CamelToColorAndroidColorMap.get(camelColor);
            Button[] buttons = buttonsByCamelColor.get(camelColor);
            for (int it = 0; it < buttons.length; ++it) {
                Button button = buttons[it];
                button.setEnabled(true);
                setButtonUnusedDiceBackground(dicesDrawableIds[it], color.normal, button);
            }
        }
    }

    private void setButtonUnusedDiceBackground(int drawableId, int targetColor, Button button) {
        setButtonDice(drawableId, targetColor, button, false);
    }

    private void setButtonUsedDiceBackground(int drawableId, int targetColor, Button button) {
        setButtonDice(drawableId, targetColor, button, true);
    }

    private void setButtonDice(int drawableId, int targetColor, Button button, boolean usedDice) {
        Bitmap dice_bitmap = BitmapFactory.decodeResource(context.getResources(), drawableId);
        Bitmap colored_dice_bitmap = replaceColor(dice_bitmap, android.graphics.Color.WHITE, targetColor);
        if (usedDice) {
            colored_dice_bitmap = replaceColor(colored_dice_bitmap, BLACK, WHITE);
        }
        BitmapDrawable colored_dice_drawable = new BitmapDrawable(context.getResources(), colored_dice_bitmap);
        button.setBackground(colored_dice_drawable);
    }
    
    private Bitmap replaceColor(Bitmap src, int fromColor, int targetColor) {
        if(src == null) {
            return null;
        }

        int width = src.getWidth();
        int height = src.getHeight();
        int[] pixels = new int[width * height];

        src.getPixels(pixels, 0, width, 0, 0, width, height);

        for(int x = 0; x < pixels.length; ++x) {
            pixels[x] = (pixels[x] == fromColor) ? targetColor : pixels[x];
        }

        Bitmap result = Bitmap.createBitmap(width, height, src.getConfig());
        result.setPixels(pixels, 0, width, 0, 0, width, height);

        return result;
    }
}
