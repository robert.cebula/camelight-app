package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Paint;
import android.graphics.Rect;

class TextPaint {
    private final Paint paint;
    private boolean calculated = false;

    TextPaint(Paint paint) {
        this.paint = paint;
    }

    void calculateTextSize(Rect rect, String text) {
        if (calculated) {
            return;
        }

        if (ScreenSize.isSmallScreen()) {
            Rect textBounds = new Rect();
            float textSize = FontUtil.calculateFontSize(textBounds, rect, text, this.paint);
            this.paint.setTextSize(textSize);
        }

        calculated = true;
    }

    Paint paint() {
        if (!calculated) throw new AssertionError(
                "You should first call calculateTextSize method of TextPaint");
        return paint;
    }
}
