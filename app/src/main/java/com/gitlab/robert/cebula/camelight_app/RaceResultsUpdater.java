package com.gitlab.robert.cebula.camelight_app;

import com.gitlab.robert.cebula.camelight_core.RaceProbabilities;
import com.gitlab.robert.cebula.camelight_core.StageProbabilities;

public interface RaceResultsUpdater {
    void updateRaceProbabilities(RaceProbabilities raceProbabilities);

    void updateStageProbabilities(StageProbabilities stageProbabilities);
}
