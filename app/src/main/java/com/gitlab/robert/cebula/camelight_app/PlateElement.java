package com.gitlab.robert.cebula.camelight_app;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;

public class PlateElement implements DrawElement {
    private final String text;
    private final Rect drawRect;
    private final Paint fillPlatePaint;
    private final Paint stepChanceFillPaint;
    private final Paint stepChanceBorderPaint;
    private final TextPaint stepChanceTextPaint;
    private final int stepChance;
    private final Paint borderPlatePaint;
    private final TextPaint plateTextPaint;

    private final Rect textBounds = new Rect(); //don't new this up in a draw method

    PlateElement(int positionChange, Rect drawRect, Paint borderPlatePaint, Paint fillPlatePaint,
                 TextPaint plateTextPaint, Paint stepChanceFillPaint, Paint stepChanceBorderPaint,
                 TextPaint stepChanceTextPaint, int stepChance) {
        this.drawRect = drawRect;
        this.fillPlatePaint = fillPlatePaint;
        this.stepChanceFillPaint = stepChanceFillPaint;
        this.stepChanceBorderPaint = stepChanceBorderPaint;
        this.stepChanceTextPaint = stepChanceTextPaint;
        this.stepChance = stepChance;
        if (positionChange > 0) {
            this.text = "+" + positionChange;
        } else {
            this.text = Integer.toString(positionChange);
        }
        this.borderPlatePaint = borderPlatePaint;
        this.plateTextPaint = plateTextPaint;
    }

    @Override
    public void draw(Canvas canvas) {
        float width = this.drawRect.width() * 0.5f;
        float height = this.drawRect.height() * 0.5f;
        float x = this.drawRect.left + (this.drawRect.width() - width) * 0.5f;
        float y = this.drawRect.top + (this.drawRect.height() - height) * 0.5f;

        RectF rectF = new RectF(x, y, x + width, y + height);
        canvas.drawRoundRect(rectF, 10, 10, this.fillPlatePaint);
        canvas.drawRoundRect(rectF, 10, 10, this.borderPlatePaint);

        Rect rect = new Rect();
        rectF.round(rect);
        rect.top += 10;
        rect.left += 10;
        rect.bottom -= 10;
        rect.right-= 10;
        this.plateTextPaint.calculateTextSize(rect, "+1");

        drawTextCentred(canvas, this.plateTextPaint.paint(), this.text, this.drawRect.centerX(), this.drawRect.centerY());

        drawStepChanceRectangle(canvas);
    }

    private void drawStepChanceRectangle(Canvas canvas) {
        float width = this.drawRect.width() * 0.3f;
        float height = this.drawRect.height() * 0.15f;
        float x = this.drawRect.left + (this.drawRect.width() - width) * 0.5f;
        float y = this.drawRect.bottom - (height * 1.05f);

        RectF rectF = new RectF(x, y, x + width, y + height);

        canvas.drawRect(rectF, this.stepChanceFillPaint);
        canvas.drawRect(rectF, this.stepChanceBorderPaint);

        String text = this.stepChance + "%";

        Rect rect = new Rect();
        rectF.round(rect);
        this.stepChanceTextPaint.calculateTextSize(rect, "100%");

        drawTextCentred(canvas, this.stepChanceTextPaint.paint(), text,
                rectF.centerX(), rectF.centerY());
    }

    private void drawTextCentred(Canvas canvas, Paint paint, String text, float cx, float cy){
        paint.getTextBounds(text, 0, text.length(), textBounds);
        canvas.drawText(text, cx - textBounds.exactCenterX(), cy - textBounds.exactCenterY(), paint);
    }
}
