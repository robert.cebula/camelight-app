package com.gitlab.robert.cebula.camelight_app;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.View;

import androidx.core.content.ContextCompat;

import com.gitlab.robert.cebula.camelight_core.Board;
import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Plate;

import java.util.HashMap;
import java.util.Map;
import com.gitlab.robert.cebula.camelight_core.Optional;

import static com.gitlab.robert.cebula.camelight_app.CamelElement.CAMEL_HEIGHT_RATIO;
import static com.gitlab.robert.cebula.camelight_app.CamelElement.CAMEL_WIDTH_RATIO;
import static com.gitlab.robert.cebula.camelight_core.BoardImpl.META_POSITION;

public class BoardView extends View {
    private final Paint fieldBoardersPaint;
    private final TextPaint plateTextPaint;
    private final Paint camelPaint;
    private final Paint borderPlatePaint;
    private final Paint camelPaintBorder;
    private final Paint fillPlatePaint;
    private final Paint stepChanceFillPaint;
    private final Paint stepChanceBorderPaint;
    private final TextPaint stepChanceTextPaint;

    private final Drawable boardDrawable;
    private final MediaPlayer plateClickPlayer;

    private int width;
    private int height;

    private int camelWidth;
    private int camelHeight;

    public final int NUMBER_OF_FIELDS_IN_ROW = 5;
    private final int NUMBER_OF_FIELDS = META_POSITION + 1;
    private Field[] fields = new Field[NUMBER_OF_FIELDS];

    private Rect canvasBounds = new Rect();
    private final StageBoardWithHistory stageBoardWithHistory;

    private final TouchManager touchManager;
    private CamelColor movingCamelColor;
    private Float movingCamelX;
    private Float movingCamelY;

    private int[] stepsInPercent = new int[NUMBER_OF_FIELDS];

    private final Map<Plate, Plate> plateTransitionsMap = new HashMap<Plate, Plate>() {{
       put(new Plate(1), new Plate(-1));
       put(new Plate(-1), null);
       put(null, new Plate(1));
    }};

    public BoardView(Context context, StageBoardWithHistory stageBoardWithHistory) {
        super(context);

        this.stageBoardWithHistory = stageBoardWithHistory;
        this.stageBoardWithHistory.addBoardUpdateListener(current -> {
            updateFields(current);
            invalidate();
        });

        this.touchManager = new TouchManager(fields, stageBoardWithHistory, this, context);

        this.fieldBoardersPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.fieldBoardersPaint.setStyle(Style.STROKE);
        this.fieldBoardersPaint.setStrokeWidth(4);
        this.fieldBoardersPaint.setColor(Color.WHITE);

        Paint plateTextPaintPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        plateTextPaintPaint.setColor(Color.WHITE);
        plateTextPaintPaint.setStyle(Style.FILL);
        plateTextPaintPaint.setTypeface(Typeface.MONOSPACE);
        plateTextPaintPaint.setTextSize(90.0f);
        this.plateTextPaint = new TextPaint(plateTextPaintPaint);

        this.camelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.camelPaint.setStyle(Style.FILL);

        this.borderPlatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.borderPlatePaint.setStyle(Style.STROKE);
        this.borderPlatePaint.setStrokeWidth(4);
        this.borderPlatePaint.setColor(Color.BLACK);

        this.fillPlatePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.fillPlatePaint .setColor(0xFF73431C);
        this.fillPlatePaint .setStyle(Style.FILL);

        this.camelPaintBorder = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.camelPaintBorder.setStyle(Style.STROKE);
        this.camelPaintBorder.setStrokeWidth(2);
        this.camelPaintBorder.setColor(Color.BLACK);

        this.stepChanceFillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.stepChanceFillPaint.setStyle(Style.FILL);
        this.stepChanceFillPaint.setColor(0xFFDDDDDD);

        this.stepChanceBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        this.stepChanceBorderPaint.setColor(Color.BLACK);
        this.stepChanceBorderPaint.setStyle(Style.STROKE);
        this.stepChanceBorderPaint.setStrokeWidth(1.0f);

        Paint stepChanceTextPaintPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        stepChanceTextPaintPaint.setColor(Color.BLACK);
        stepChanceTextPaintPaint.setStyle(Style.FILL);
        stepChanceTextPaintPaint.setTypeface(Typeface.MONOSPACE);
        stepChanceTextPaintPaint.setTextSize(32.0f);
        this.stepChanceTextPaint = new TextPaint(stepChanceTextPaintPaint);

        this.boardDrawable = ContextCompat.getDrawable(context, R.drawable.board);

        this.plateClickPlayer = MediaPlayer.create(context, R.raw.plate_click);
    }

    public void updateSteps(int[] newSteps) {
        int sum = 0;
        for (int newStep : newSteps) {
            sum += newStep;
        }

        for (int it = 0; it < newSteps.length; ++it) {
            float percentage = (float)newSteps[it] / (float)sum;
            this.stepsInPercent[it] = Math.round(percentage * 100.0f);
        }
    }

    private void constructFields() {
        int fieldWidth = this.width / NUMBER_OF_FIELDS_IN_ROW;
        int fieldHeight = this.height / NUMBER_OF_FIELDS_IN_ROW;
        camelWidth = (int)(fieldWidth * CAMEL_WIDTH_RATIO);
        camelHeight = (int)(fieldHeight * CAMEL_HEIGHT_RATIO);

        // this is position of upper left field from which we start creating our board
        int position = 10;
        // upper fields
        int upperFieldY = 0;
        for (int field = 0; field < NUMBER_OF_FIELDS_IN_ROW; ++field) {
            int x = fieldWidth * field;

            Rect rect = new Rect(x, upperFieldY, x + fieldWidth, upperFieldY + fieldHeight);
            fields[position % NUMBER_OF_FIELDS] = new Field(rect, plateTextPaint,
                    camelPaint, borderPlatePaint, camelPaintBorder, fillPlatePaint,
                    stepChanceFillPaint, stepChanceBorderPaint, stepChanceTextPaint);
            ++position;
        }

        // right fields
        int rightFieldX = fieldWidth * (NUMBER_OF_FIELDS_IN_ROW - 1);
        for (int field = 1; field < NUMBER_OF_FIELDS_IN_ROW; ++field) {
            int y = fieldHeight * field;

            Rect rect = new Rect(rightFieldX, y, rightFieldX + fieldWidth, y + fieldHeight);
            fields[position % NUMBER_OF_FIELDS] = new Field(rect, plateTextPaint,
                    camelPaint, borderPlatePaint, camelPaintBorder, fillPlatePaint,
                    stepChanceFillPaint, stepChanceBorderPaint, stepChanceTextPaint);
            ++position;
        }

        // bottom fields (from right to left)
        int bottomFieldY = fieldHeight * (NUMBER_OF_FIELDS_IN_ROW - 1);
        for (int field = NUMBER_OF_FIELDS_IN_ROW - 2; field >= 0; --field) {
            int x = fieldWidth * field;

            Rect rect = new Rect(x, bottomFieldY, x + fieldWidth, bottomFieldY + fieldHeight);
            fields[position % NUMBER_OF_FIELDS] = new Field(rect, plateTextPaint,
                    camelPaint, borderPlatePaint, camelPaintBorder, fillPlatePaint,
                    stepChanceFillPaint, stepChanceBorderPaint, stepChanceTextPaint);
            ++position;
        }

        // left fields (from bottom to up)
        int leftFieldX = 0;
        for (int field = NUMBER_OF_FIELDS_IN_ROW - 2; field >= 1; --field) {
            int y = fieldHeight * field;

            Rect rect = new Rect(leftFieldX, y, leftFieldX + fieldWidth, y + fieldHeight);
            fields[position % NUMBER_OF_FIELDS] = new Field(rect, plateTextPaint,
                    camelPaint, borderPlatePaint, camelPaintBorder, fillPlatePaint,
                    stepChanceFillPaint, stepChanceBorderPaint, stepChanceTextPaint);
            ++position;
        }

        updateFields(stageBoardWithHistory);
    }

    private void updateFields(Board board) {
        for (int position = 0; position < fields.length; ++position) {
            fields[position].updateDrawElement(board.getBoardElement(position), stepsInPercent[position]);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        this.width = w;
        this.height = h;

        constructFields();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.getClipBounds(canvasBounds);
        boardDrawable.setBounds(canvasBounds);
        boardDrawable.draw(canvas);

        drawBoard(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        touchManager.onTouchEvent(event);

        if (event.getAction() != MotionEvent.ACTION_DOWN) {
            return true;
        }

        Optional<Integer> position = getClickedFieldPosition(event);
        if (!position.isPresent()) {
            return false;
        }

        Optional<Plate> currentPlate = stageBoardWithHistory.getPlateAtPosition(position.get());

        Plate nextPlate = getNextPlate(currentPlate.orElse(null));
        if (!stageBoardWithHistory.addOrSwitchPlateAtPosition(nextPlate, position.get())) {
            return true;
        }

        plateClickPlayer.start();

        return true;
    }

    private Plate getNextPlate(Plate plate) {
        return plateTransitionsMap.get(plate);
    }

    private Optional<Integer> getClickedFieldPosition(MotionEvent event) {
        for (int it = 0; it < fields.length; ++it) {
            if (fields[it].isClicked((int)event.getX(), (int)event.getY())) {
                return Optional.of(it);
            }
        }

        return Optional.empty();
    }

    private void drawBoard(Canvas canvas) {
        for (Field field : fields) {
            field.draw(canvas);
        }

        drawMovingCamel(canvas);
    }

    public void updateMovingCamel(CamelColor camelColor, Float x, Float y) {
        movingCamelColor = camelColor;
        movingCamelX = x;
        movingCamelY = y;
    }

    private void drawMovingCamel(Canvas canvas) {
        if (movingCamelColor != null) {
            this.camelPaint.setColor(CamelToColorAndroidColorMap.get(movingCamelColor).dimmed);
            canvas.drawRect(movingCamelX, movingCamelY, movingCamelX + camelWidth,
                    movingCamelY + camelHeight, this.camelPaintBorder);
            canvas.drawRect(movingCamelX, movingCamelY, movingCamelX + camelWidth,
                    movingCamelY + camelHeight, this.camelPaint);
        }
    }
}
