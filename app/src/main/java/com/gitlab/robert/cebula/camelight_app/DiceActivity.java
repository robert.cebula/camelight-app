package com.gitlab.robert.cebula.camelight_app;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.TypedValue;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gitlab.robert.cebula.camelight_core.CamelColor;
import com.gitlab.robert.cebula.camelight_core.Dice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DiceActivity extends Activity {

    private LinearLayout dicesVerticalLayout;
    private TextView appNameTextView;
    private TextView authorTextView;
    private TextView mailTextView;

    private Button goButton;

    private Map<CamelColor, Integer> diceValueByCamelColor;

    private MediaPlayer buttonClickPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScreenSize.init(this);

        // TODELETE
        // only for testing purpose
//        runGameActivity(new ArrayList<Dice>() {{
//            add(new Dice(CamelColor.BLUE, 1));
//            add(new Dice(CamelColor.ORANGE, 3));
//            add(new Dice(CamelColor.WHITE, 2));
//            add(new Dice(CamelColor.GREEN, 2));
//            add(new Dice(CamelColor.YELLOW, 3));
//        }});

        setContentView(R.layout.dice_activity);

        dicesVerticalLayout = findViewById(R.id.dices_vertical_layout);

        this.diceValueByCamelColor = new HashMap<>();
        for (CamelColor camelColor : CamelColor.values()) {
            this.diceValueByCamelColor.put(camelColor, null);
        }

        this.buttonClickPlayer = MediaPlayer.create(this, R.raw.button_click);

        goButton = findViewById(R.id.dices_action_button);
        goButton.setEnabled(false);
        goButton.setOnClickListener(view -> {
            this.buttonClickPlayer.start();
            ArrayList<Dice> startingDices = new ArrayList<>();
            for (CamelColor camelColor : diceValueByCamelColor.keySet()) {
                Integer value = diceValueByCamelColor.get(camelColor);

                startingDices.add(new Dice(camelColor, value + 1));
            }

            StringBuilder msg = new StringBuilder();
            for (Dice dice : startingDices) {
                msg.append(dice.camelColor()).append(":").append(dice.value()).append("\n");
            }

            runGameActivity(startingDices);
        });

        this.appNameTextView = findViewById(R.id.app_name_text_view);
        this.authorTextView = findViewById(R.id.author_text_view);
        this.mailTextView = findViewById(R.id.mail_text_view);

        createDicesLayout();

        if (ScreenSize.isSmallScreen()) {
            this.appNameTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 32.0f);
            goButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 32.0f);
            this.authorTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);
            this.mailTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);
        }
        if (ScreenSize.isTinyScreen()) {
            this.goButton.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 24.0f);
            this.authorTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);
            this.mailTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16.0f);
        }
    }

    private void runGameActivity(ArrayList<Dice> startingDices) {
        Intent intent = new Intent(DiceActivity.this, GameActivity.class);
        intent.putExtra("startingDices", startingDices);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    private void dicesController(CamelColor camelColor, Integer newValue) {
        this.diceValueByCamelColor.put(camelColor, newValue);

        if (!this.diceValueByCamelColor.values().contains(null)) {
            goButton.setEnabled(true);
        }
    }

    private void createDicesLayout() {
        for (CamelColor camelColor : CamelColor.values()) {
            createDicesRow(camelColor);
        }
    }

    private void createDicesRow(final CamelColor camelColor) {
        LinearLayout linearLayout = new LinearLayout(this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                0,
                1.0f);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setWeightSum(3.0f);

        final Button[] buttonsInRow = new Button[Dice.MAX_VALUE];
        for (int it = 0; it < Dice.MAX_VALUE; ++it) {
            Button button = new Button(this);
            layoutParams = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f);
            if (ScreenSize.isTinyScreen()) {
                layoutParams.setMargins(10, 10, 10, 10);
            } else if (ScreenSize.isSmallScreen()) {
                layoutParams.setMargins(20, 20, 20, 20);
            } else {
                layoutParams.setMargins(40, 40, 40, 40);
            }
            button.setLayoutParams(layoutParams);
            button.setText(Integer.toString(it+1));
            button.setTextSize(24.0f);
            button.setTextColor(android.graphics.Color.BLACK);
            final Color color = CamelToColorAndroidColorMap.get(camelColor);
            button.setBackgroundColor(color.dimmed);
            buttonsInRow[it] = button;
            final int ourIt = it;
            button.setOnClickListener(view -> {
                this.buttonClickPlayer.start();
                for (int it1 = 0; it1 < Dice.MAX_VALUE; ++it1) {
                    Button buttonToModify = buttonsInRow[it1];
                    if (it1 == ourIt) {
                        buttonToModify.setBackgroundColor(color.normal);
                        dicesController(camelColor, it1);
                    } else {
                        buttonToModify.setBackgroundColor(color.dimmed);
                    }
                }
            });
            linearLayout.addView(button);
        }

        dicesVerticalLayout.addView(linearLayout);
    }
}
